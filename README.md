# Training materials for the MINERVA Platform

## Reading material:

[The MINERVA Platform website](https://minerva.pages.uni.lu)

[MINERVA REST API documentation](https://minerva.pages.uni.lu/doc/api/15.1/index/)

