# Reproducible exploration and analysis of disease maps
## MIRIADE Trainig Week, 2021

### Prerequisites:
- R [https://cran.r-project.org](https://cran.r-project.org)
- RStudio [https://www.rstudio.com](https://www.rstudio.com)
- A text editor with JSON syntax (e.g. Visual Studio Code [https://code.visualstudio.com/download](https://code.visualstudio.com/download))

### **Setup**
Run `Task_0_setup.R` to install all necessary packages and import MINERVA helper functions.

### **Task 1**
Explore bioentities and their annotations in the [Dementia Disease Map](https://miriade.lcsb.uni.lu), both visually and programmatically.
Datasets required for the analysis (VUMC and KTH) will be provided separately.

### **Task 2**
Retrieve information about drugs targeting biomarkers in the Map.

### **Task 3**
Expand the contents of the map based on the biomarker datasets and text mining

