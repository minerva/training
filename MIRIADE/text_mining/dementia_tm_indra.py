from indra.sources import indra_db_rest
from indra.sources.indra_db_rest.query import *
from indra.tools.assemble_corpus import filter_genes_only
from indra.statements.statements import stmts_to_json
import json
import pickle

dirpath = './'

# A function to query INDRA REST API for statements from PMID abstracts.
# It is paginated to control the size of the queries and to save intermediate results
def indra_paginated_pmids(all_pmids, window):
    res = []
    for i in range(0,len(all_pmids),window):
        qres = indra_db_rest.get_statements_for_papers([('pmid', x) for x in all_pmids[i:i + window]],tries = 10)
        ### Only gene-level entries
        res = res + filter_genes_only(qres.statements)
        pickle.dump(res, file = open(dirpath+'indra_pmid_query_progress.pkl', 'wb'))
        print(str(i)+" ---\n")
    qres = indra_db_rest.get_statements_for_papers([('pmid', x) for x in all_pmids[i:len(all_pmids)]])
    res = res + qres.statements
    return(res)

### PMIDS from a query "biomarker*[Title/Abstract] AND dementia[Title/Abstract]"
filename = 'pmids_biomarkers_AND_dementia.txt'

### Read the file
with open(filename) as file:
    pmid_universe = file.readlines()
    pmid_universe = [line.rstrip() for line in pmid_universe]

pmid_universe = list([x for x in pmid_universe if x == x])

output = indra_paginated_pmids(all_pmids=pmid_universe, window=500)

pickle.dump(output, 
            file = open(dirpath+'indra_pmid_query.pkl', 'wb'))

### Consolidate the statements
from collections import defaultdict
stmts_by_hash = {stmt.get_hash(): stmt for stmt in output}
merged_evidences = defaultdict(list)
for stmt in output:
    merged_evidences[stmt.get_hash()] += stmt.evidence
for sh, stmt in stmts_by_hash.items():
    stmt.evidence = merged_evidences[sh]
merged_stmts = list(stmts_by_hash.values())

with open(dirpath + 'dementia_indra_statements.json', 'w') as outfile:
    json.dump(stmts_to_json(merged_stmts), outfile)
