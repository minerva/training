### Login to an instance of MINERVA Platform
# curl -X POST -c - --data "login=anonymous&password=" https://minerva-dev.lcsb.uni.lu/minerva/api/doLogin
login <- function(furl, login = NULL, password = NULL, verbose = F) {
  logstring <- "login=anonymous&password="
  if(!is.null(login)) {
    logstring <- paste0("login=", login,  "&password=",  password)
  }
  auth <- httr::POST(url = paste0(furl, "doLogin"),
                     httr::add_headers('Content-Type' = "application/x-www-form-urlencoded"),
                     body = logstring,
                     httr::config(verbose = verbose))
  return(httr::cookies(auth)$value[1])
}

### A convenience function to handle API GET queries that may require a token
ask_GET <- function(fask_url, token = "", verbose = F, encoding = "UTF-8", ctype ="application/json") {
  if(verbose) {
    message(utils::URLencode(fask_url))
  }
  resp <- httr::GET(url = fask_url,
                    httr::add_headers('Content-Type' = ctype),
                    httr::set_cookies(MINERVA_AUTH_TOKEN = token),
                    httr::config(verbose = verbose))
  if(httr::status_code(resp) == 200) {
    ### when the content is sent as a zip file, it needs to be handled differently,
    ### i.e. saved to a tmp file and unzipped
    if(httr::headers(resp)$`content-type` == "application/zip") {
      tmp <- tempfile()
      tmpc <- file(tmp, "wb")
      writeBin(httr::content(resp, as = "raw"), con = tmpc)
      close(tmpc)
      unzipped <- utils::unzip(tmp)
      file.remove(tmp)
      return(unzipped)
    } else {
      return(httr::content(resp, as = "text", encoding = encoding))
    }
  } else {
    message("API call error:")
    message(resp)
  }
  return(NULL)
}