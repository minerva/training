# Reproducible exploration and analysis of disease maps
## WGCAC 2022

### Prerequisites:
- R [https://cran.r-project.org](https://cran.r-project.org)
- RStudio [https://www.rstudio.com](https://www.rstudio.com)
- A text editor with JSON syntax (e.g. Visual Studio Code [https://code.visualstudio.com/download](https://code.visualstudio.com/download))

### **Setup**
Run `Task_0_setup.R` to install all necessary packages and access the contents of the SYSCID Disease Map.

### **Tasks - to be provided**

### Suggested materials
- Video tutorials on the [website of the Asthma Map](https://asthma-map.org/tutorials/)
- About disease maps [PMID:29872544](https://pubmed.ncbi.nlm.nih.gov/29872544/), [PMID:29688273](https://pubmed.ncbi.nlm.nih.gov/29688273/)
- About the MINERVA Platform [PMID:28725475](https://pubmed.ncbi.nlm.nih.gov/28725475/)
- About the MINERVA API [PMID:31074494](https://pubmed.ncbi.nlm.nih.gov/31074494/)

