This set of coded excercises focuses on the COVID-19 Disease Map, and its programmatic access.
File `practical_crosstalks.R` contains the following workflow:
- Map access
  - access the map on the MINERVA Platform using REST API (wrapped in existing helper functions)
  - download annoataions of all diagrams in the map
  - create lists of HGNC identifirs per diagram
- Interaction databases
  - load the INDRA EMMAA text mining data
  - filter by belief and trim down to HGNC-HGNC interactions
  - load OmniPathDB and intersect with EMMAA statements
- Crosstalk
  - Filter EMMAA-OP interactions to cross-talk between diagrams
  - Generate crosstalk graph
- Cytoscape > MINERVA
  - Create a XGMML object from Cytoscape based on the crosstalk graph
  - Convert to CellDesigner format